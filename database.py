import mysql.connector
import yaml


class Database:
    def __init__(self):
        with open('database_config.yaml', 'r') as f:
            config = yaml.load(f, Loader=yaml.FullLoader)

        self.mydb = mysql.connector.connect(
            host=config['host'],
            user=config['user'],
            password=config['password'],
            database=config['database'],
        )
        self.mycursor = self.mydb.cursor()

    def list_users(self):
        self.mycursor.execute("SELECT email FROM Persons")
        result = self.mycursor.fetchall()
        self.mycursor.close()
        self.mydb.close()
        return result

    def listCourseMates(self, course):
        self.mycursor.close()
        self.mydb.close()
        self.mydb = self.login_to_database()
        self.mycursor = self.mydb.cursor()
        self.mycursor.execute("SELECT firstName, lastName, p.email FROM courses c INNER JOIN persons p ON c.email = p.email WHERE c.course =\"{}\"".format(course))
        result = self.mycursor.fetchall()
        return result

    def get_user(self, info):
        self.mycursor.close()
        self.mydb.close()
        self.mydb = self.login_to_database()
        self.mycursor = self.mydb.cursor()
        self.mycursor.execute(
            "SELECT s.email, s.password, s.firstName FROM Persons s WHERE s.email = \"{}\"".format(info))
        result = self.mycursor.fetchall()
        return result

    def getInfo(self, email):
        self.mycursor.close()
        self.mydb.close()
        self.mydb = self.login_to_database()
        self.mycursor = self.mydb.cursor()
        self.mycursor.execute(
            "SELECT * FROM Persons s WHERE s.email = \"{}\"".format(email))
        result = self.mycursor.fetchall()
        return result

    def login_to_database(self):
        with open('database_config.yaml', 'r') as f:
            config = yaml.load(f, Loader=yaml.FullLoader)

        mydb = mysql.connector.connect(
            host=config['host'],
            user=config['user'],
            password=config['password'],
            database=config['database'],
        )
        return mydb

    def addUser(self, firstName, lastName, email, address, postalCode, password):
        self.mydb = self.login_to_database()
        self.mycursor = self.mydb.cursor()
        message = None
        # Check for existing email
        self.mycursor.execute(
            "SELECT s.email FROM Persons s WHERE s.email = \"{}\"".format(email))
        result = self.mycursor.fetchall()
        if len(result) == 0:
            query = "INSERT INTO Persons (firstName, lastName, email, address, postalName, password) VALUES (%s, %s, %s, %s, %s, %s)"
            val = [(firstName, lastName, email, address, postalCode, password)]
            try:
                self.mycursor.executemany(query, val)
                self.mydb.commit()
            except mysql.connector.Error as err:
                print(err)
                print("Error Code:", err.errno)
                print("SQLSTATE", err.sqlstate)
                print("Message", err.msg)
        else:
            message = "Account with email already exist."
        # self.mycursor.close()
        self.mydb.close()
        return message

    def add_new_class(self, email, course_name):
        self.mydb = self.login_to_database()
        self.mycursor = self.mydb.cursor()
        message = None
        # Check for existing email
        self.mycursor.execute(
            "SELECT s.email FROM clone s WHERE s.course_name = \"{}\"".format(course_name))
        result = self.mycursor.fetchall()
        if len(result) == 0:
            query = "INSERT INTO clone (email, course_name) VALUES (%s, %s)"
            val = [(email, course_name)]
            try:
                self.mycursor.executemany(query, val)
                self.mydb.commit()
            except mysql.connector.Error as err:
                print(err)
                print("Error Code:", err.errno)
                print("SQLSTATE", err.sqlstate)
                print("Message", err.msg)
        else:
            message = "You have previously registered for this course."
        # self.mycursor.close()
        self.mydb.close()
        return message

    def upload_image(self, email, image):
        message = None
        self.mydb = self.login_to_database()
        self.mycursor = self.mydb.cursor()
        # Check for existing email
        self.mycursor.execute(
            "SELECT s.email FROM Pictures s WHERE s.email = \"{}\"".format(email))
        result = self.mycursor.fetchall()
        if len(result) == 0:
            query = "INSERT INTO Pictures (email, photo) VALUES (%s, %s)"
            val = [(email, image)]
            try:
                self.mycursor.executemany(query, val)
                self.mydb.commit()
            except mysql.connector.Error as err:
                print(err)
                print("Error Code:", err.errno)
                print("SQLSTATE", err.sqlstate)
                print("Message", err.msg)
        elif len(result) != 0:
            self.mycursor.execute(
                "DELETE FROM Pictures WHERE email = \"{}\"".format(email))
            query = "INSERT INTO Pictures (email, photo) VALUES (%s, %s)"
            val = [(email, image)]
            self.mycursor.executemany(query, val)
            self.mydb.commit()
        else:
            message = 'Error'
        self.mycursor.close()
        self.mydb.close()
        return message

    def retreive_image(self, email):
        message = None
        self.mydb = self.login_to_database()

    def readBLOB(self, emp_id):
        response = None
        try:
            message = None
            with open('database_config.yaml', 'r') as f:
                config = yaml.load(f, Loader=yaml.FullLoader)

            connection = mysql.connector.connect(
                host=config['host'],
                user=config['user'],
                password=config['password'],
                database=config['database'],
                raw=True
            )
            cursor = connection.cursor()
            sql_fetch_blob_query = """SELECT * from Pictures where email = %s"""

            cursor.execute(sql_fetch_blob_query, (emp_id,))
            record = cursor.fetchall()

            if len(record) == 0:
                response = "No photo for user"

            else:
                response = record[0][1]

        except mysql.connector.Error as error:
            print("Failed to read BLOB data from MySQL table {}".format(error))

        finally:
            if (connection.is_connected()):
                cursor.close()
                connection.close()
                print("MySQL connection is closed")
        return response

    def addCourse(self, email, courseId):
        self.mydb = self.login_to_database()
        self.mycursor = self.mydb.cursor()
        message = None
        # courseFormat = courseId.split(" ")
        # courseId = "{}{}".format(courseId[0], courseId[1])
        # Check for existing email
        self.mycursor.execute(
            "SELECT s.course FROM Courses s WHERE s.course = \"{}\" AND s.email = \"{}\"".format(courseId, email))
        result = self.mycursor.fetchall()
        if len(result) == 0:
            query = "INSERT INTO Courses (email, course) VALUES (%s, %s)"
            val = [(email, courseId)]
            try:
                self.mycursor.executemany(query, val)
                self.mydb.commit()
            except mysql.connector.Error as err:
                print(err)
                print("Error Code:", err.errno)
                print("SQLSTATE", err.sqlstate)
                print("Message", err.msg)
        else:
            message = "Course already added."
        # self.mycursor.close()
        self.mydb.close()
        return message

    def deleteCourse(self, email, courseId):
        self.mydb = self.login_to_database()
        self.mycursor = self.mydb.cursor()
        message = None
        # courseFormat = courseId.split(" ")
        # courseId = "{}{}".format(courseId[0], courseId[1])
        # Check for existing email
        query = "DELETE FROM Courses WHERE email=\"{}\" AND course = \"{}\"".format(
            email, courseId)
        try:
            self.mycursor.execute(query)
            self.mydb.commit()
        except mysql.connector.Error as err:
            print(err)
            print("Error Code:", err.errno)
            print("SQLSTATE", err.sqlstate)
            print("Message", err.msg)
        else:
            message = "Deleted"
        self.mydb.close()
        return message

    def getCourses(self, email):
        self.mycursor.close()
        self.mydb.close()
        self.mydb = self.login_to_database()
        self.mycursor = self.mydb.cursor()
        self.mycursor.execute(
            "SELECT s.course FROM Courses s WHERE s.email = \"{}\"".format(email))
        result = self.mycursor.fetchall()
        return result

    def updateStatus(self, email, stat):
        self.mycursor.close()
        self.mydb.close()
        self.mydb = self.login_to_database()
        self.mycursor = self.mydb.cursor()
        # Check for existing email
        self.mycursor.execute(
            "SELECT * FROM status WHERE email = \"{}\"".format(email))
        result = self.mycursor.fetchall()
        if len(result) == 0:
            query = "INSERT INTO status (email, active) VALUES (%s, %s)"
            val = [(email, stat)]
            try:
                self.mycursor.executemany(query, val)
                self.mydb.commit()
            except mysql.connector.Error as err:
                print(err)
                print("Error Code:", err.errno)
                print("SQLSTATE", err.sqlstate)
                print("Message", err.msg)
        else:
            query = "UPDATE status SET active = \"{}\" WHERE email = \"{}\"".format(stat, email)
            try:
                self.mycursor.execute(query)
                self.mydb.commit()
            except mysql.connector.Error as err:
                print(err)
                print("Error Code:", err.errno)
                print("SQLSTATE", err.sqlstate)
                print("Message", err.msg)
        # self.mycursor.close()
        self.mydb.close()

    def getStatusList(self, email):
        self.mycursor.close()
        self.mydb.close()
        self.mydb = self.login_to_database()
        self.mycursor = self.mydb.cursor()
        self.mycursor.execute(
            "SELECT * FROM status s WHERE s.email = \"{}\"".format(email))
        result = self.mycursor.fetchall()
        return result



def main():
    users = Database()

main()
