function Settings() {
    var action = document.getElementById("settings").value;
    if (action == 'Change_Picture') {
        $("#uploader").show();
    };
    if (action == 'Log_Out') {
        window.location = window.location.origin;
    };
};

$("#noAccount").click(function() {
    $(".loginImage").hide();
    $(".signUpForm").show();
    $("#loginForm").hide();
    $(".loginImageRight").show();
    $(".return").show();
});

$(".return").click(function() {
    $(".loginImage").show();
    $(".signUpForm").hide();
    $("#loginForm").show();
    $(".loginImageRight").hide();
    $(".return").hide();
});

$(".uploadPic").click(function() {
    $(".uploadDiv").show();
});

$(".showMessages").click(function() {
    $(".uploadDiv").hide();
    $(".messageDiv").show();
    $(".add_course").show();
});


$(document).ready(function () {
    let naming = nom;
    $(".studentList").hide();
    var retreive = JSON.parse(courseList);
    let firstCourse = retreive['courses'][0];
    fillCourseMembers(firstCourse);

    $("#messageCourseName").text(firstCourse);
    $("#member").click(function() {
        $(".messagingBox").hide();
        $(".studentList").show();
    });

    $("#active").click(function() {
        $(".studentList").hide();
        $(".messagingBox").show();
    });

    for (var i = 0; i < retreive['courses'].length; i++) {
        let course = retreive['courses'][i];
        // Finding total number of elements added
        var total_element = $(".element").length;
        // last <div> with element class id
        var lastid = $(".element:last").attr("id");
        var split_id = lastid.split("_");
        var nextindex = Number(split_id[1]) + 1;
        var max = 10;

        // Adding new div container after last occurance of element class
        $(".element:last").after("<div class='element' id='div_" + nextindex + "'></div>");

        // Adding element to <div>
        $("#div_" + nextindex).append("<p id='vev_" + nextindex + "'> " + course + "</p>&nbsp;<div id='join_" + nextindex + "' class='join'>Join</div>&nbsp;<div id='remove_" + nextindex + "' class='remove'>Remove</div><hr>");
        $("#div_" + nextindex).width("80%");
        $("#div_" + nextindex).css('background-color', 'white');
        $("#vev_" + nextindex).css('text-align', 'center');
        $("#vev_" + nextindex).css('margin', '0px');
        $("#remove_" + nextindex).css('float', 'right');
        $("#join_" + nextindex).css('float', 'left');
    }

    // Add new element
    $(".add").click(function () {

        // Finding total number of elements added
        var total_element = $(".element").length;
        // last <div> with element class id
        var lastid = $(".element:last").attr("id");
        var split_id = lastid.split("_");
        var nextindex = Number(split_id[1]) + 1;
        let course = document.getElementById('txt_1').value;
        var max = 10;

        // Adding new div container after last occurance of element class
        $(".element:last").after("<div class='element' id='div_" + nextindex + "'></div>");

        // Adding element to <div>
        $("#div_" + nextindex).append("<p id='vev_" + nextindex + "'> " + course + "</p>&nbsp;<div id='join_" + nextindex + "' class='join'>Join</div>&nbsp;<div id='remove_" + nextindex + "' class='remove'>Remove</div><hr>");
        $("#div_" + nextindex).width("80%");
        //$("#div_" + nextindex).css('border-style', 'solid');
        //$("#div_" + nextindex).css('border-color', 'coral');
        $("#vev_" + nextindex).css('text-align', 'center');
        //$("#vev_" + nextindex).css('color', 'whitw');
        $("#vev_" + nextindex).css('margin', '0px');
        $("#remove_" + nextindex).css('float', 'right');
        $("#join_" + nextindex).css('float', 'left');
        send_course(course)

    });

    // Join a class
    $('.add_course').on('click', '.join', function () {

        var id = this.id;
        var split_id = id.split("_");
        var joinIndex = split_id[1];
        let courseToJoin = $("#vev_" + joinIndex).text();
        $("#messageCourseName").empty();
        $("#messageCourseName").text(courseToJoin);
        fillCourseMembers(courseToJoin);
    });

    function fillCourseMembers(course){
        $.ajax({
            type: 'GET',
            url: '/courseMembers',
            dataType: 'json',
            data: { data: JSON.stringify([course]) },
            success: function(res) {
                $(".studentList").empty();
                for (const [key, value] of Object.entries(res)){
                    if(value == "on"){
                        $(".studentList").append('<div style="margin-bottom: 5px"><b>'+ '<span class="dot" style="background-color: #00FF00"> </span>' + " " + key + '</div>');
                    }else{
                        $(".studentList").append('<div style="margin-bottom: 5px"><b>'+ '<span class="dot" style="background-color: #FF0000"> </span>' + " " + key + '</div>');
                    }
                }
            }
        });
    }
        // Remove element
    $('.add_course').on('click', '.remove', function () {

        var id = this.id;
        var split_id = id.split("_");
        var deleteindex = split_id[1];
        let courseToDelete = $("#vev_" + deleteindex).text();
        // Remove <div> with id
        $("#div_" + deleteindex).remove();
        let info = [email, courseToDelete];
        $.ajax({
            type: 'POST',
            url: '/removeCourse',
            dataType: 'json',
            data: { data: JSON.stringify(info) },
            success: function (res) {
            }
        });

    });


    var socket = io.connect('http://' + document.domain + ':' + location.port);
    let messageDict = {
        'Van': 'Hello guys',
        'James': 'Hey Buddy',
        'Sam': 'Got assignments'
    }

    let messa = {
        'Sam': 'Got assignments'
    }

    for (const [key, value] of Object.entries(messageDict)) {
        $('div.messagingBox').append('<div style="margin-bottom: 10px; text-align:left;"><b>' + '<img src="/static/img/view.jpg" style="width:30px; height:30px;">' + '   '+ value + '</div>')
    }
    socket.on('connect', function () {
        socket.emit('my event', {
            data: naming + ' connected'
        })
        $('#messageForm').submit(function (e) {
            e.preventDefault()
            let user_name = naming;
            let user_input = $('input.message').val()
            socket.emit('my event', {
                user_name: user_name,
                message: user_input
            })
            $('input.message').val('').focus()
        })
    })
    socket.on('my response', function (msg) {
        let picc = pp;
        if (typeof msg.user_name !== 'undefined') {
            $('div.messagingBox').prepend('<div style="background:#F8F8F8; margin-bottom: 5px; text-align:right;"><b>' + msg.message + '    ' + '<img src="'+picc+'" style="width:30px; height:30px;">' + '</div>')
        }
    })

    //let check_image = document.getElementById("profile").src;
    //let pic_path = pic_link.split("/");
    //if (pic_link != null || pic_path[-1] != 'profile.png') {
    //    $("#uploader").hide();
    //};


    //// Get value


    const inputElement = document.getElementById("input");
    inputElement.addEventListener("change", handleFiles, false);

    function handleFiles() {
        const fileList = this.files; /* now you can work with the file list */
        //console.log(this.files.length);
        const numFiles = this.files.length;
        for (let i = 0, numFiles = this.files.length; i < numFiles; i++) {
            const file = this.files[i];
            let image = document.getElementById("profile");
            image.src = URL.createObjectURL(file);
            let info = [email, image.src];
            console.log(image.src)
            $.ajax({
                type: 'POST',
                url: '/image',
                data: { data: JSON.stringify(info) },
                dataType: 'json',
            });
        }
    }

    //function setUp(course){}

    function makeCourseDiv(course) {
        var node = document.createElement("LI");
        node.id = "courseListId";
        var courseDiv = document.createElement("div");
        courseDiv.id = "courses";
        var joinBtn = document.createElement("button");
        joinBtn.id = "join";
        joinBtn.innerHTML = "Join";
        var removeBtn = document.createElement("button");
        removeBtn.id = "remove";
        removeBtn.innerHTML = "Remove";
        var paragraph = document.createElement("P");
        paragraph.id = "courseName";
        var textnode = document.createTextNode(course);
        paragraph.appendChild(textnode)
        courseDiv.appendChild(paragraph)
        courseDiv.appendChild(joinBtn);
        courseDiv.appendChild(removeBtn);
        node.appendChild(courseDiv);
        document.getElementById("myList").appendChild(node);
    }

    function send_course(course) {
        let info = [email, course];
        $.ajax({
            type: 'POST',
            url: '/course',
            data: { data: JSON.stringify(info) },
            dataType: 'json',
        });
    }

    // update profile picture
    if (pic_link != null) {
        let image = document.getElementById("profile");
        image.src = pic_link;
    };

    function fillCourse() {
        // Retrieve user courses
        let info = [$("#mail").val()];
        $.ajax({
            type: 'POST',
            url: '/getCourses',
            dataType: 'json',
            data: { data: JSON.stringify(info) },
            success: function (res) {
                $(".messageDiv").hide();
            }
        });
    }
});