from flask import Flask, render_template, redirect, url_for, request, session, jsonify, send_from_directory, send_file
from flask_socketio import SocketIO
from werkzeug.utils import secure_filename
from urllib.request import urlopen
from base64 import b64encode
from database import Database
import glob
import json
import os
import shutil
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = os.path.join(os.getcwd(), 'static')
app.config['SECRET_KEY'] = '#2703ARAD'
socket = SocketIO(app)
e_mail = ""
database = Database()
currentSession = []


def configureResponse(courseList):
    refine = []
    for tple in courseList:
        refine.append(tple[0])
    return refine

@app.route('/')
@app.route('/<error>')
def launch(error=""):
    return render_template('index.html', error=error)


@app.route('/signup')
@app.route('/signup/<error>')
def signup(error=""):
    return render_template('signup.html', error=error)


@app.route('/user_signup', methods=['GET', 'POST'])
def user_signup():
    message = ""
    if request.method == 'POST':
        firstName = request.form['firstName']
        lastName = request.form['lastName']
        useremail = request.form['email']
        address = request.form['address']
        postalCode = request.form['postalCode']
        password = request.form['password']
        message = check_email(useremail)
        if message is None:
            message = database.addUser(firstName, lastName, useremail,
                     address, postalCode, password)
        if message is not None:
            return redirect(url_for('signup', error=message))
    return redirect(url_for('profile', name=useremail))

def createNewUser(firstName, lastName, useremail, address, postalCode, password):
    database.addUser(firstName, lastName, useremail,
                     address, postalCode, password)


def check_email(email):
    message = None
    if '@mun' not in email:
        message = 'Invalid email. Please try again'
    return message


@app.route('/user', methods=['GET', 'POST'])
def user():
    if request.method == 'POST':
        useremail = request.form['email']
        password = request.form['password']
        message = verify(useremail, password)
        if message is not None:
            return redirect(url_for('launch', error=message))
    return redirect(url_for('profile', name=useremail))


def file_upload_check(file):
    result = None
    if not file.endswith('jpg'):
        result = 'Invalid'
    elif file.endswith('png'):
        result = 'Invalid format'
    elif file is None:
        result = 'No file'
    return result


def convertToBinaryData(filename):
    # Convert digital data to binary format
    with open(filename, 'rb') as file:
        binaryData = file.read()
    return binaryData


@app.route('/profile/<name>/upload', methods=['GET', 'POST'])
def upload(name, methods=['GET', 'POST']):
    info = [name, name]
    pic = None
    statusInfo = database.getStatusList(name)
    statusActive = statusInfo[0][1]
    if(statusActive != "on"):
        return redirect(url_for('launch', error="Sign in"))
    if request.method == 'POST':
        image_file = request.files['file']
        # Check uploaded file
        if image_file.filename == '':
            # flash('No file selected for uploading')
            return render_template('user.html', info=info, picture=None)
        if image_file:
            filename = secure_filename(image_file.filename)
            image_file.save(os.path.join(
                app.config['UPLOAD_FOLDER'], filename))
            pic = os.path.join('static', filename)
            n_pic = pic.replace(os.sep, '/')
            empPicture = convertToBinaryData(n_pic)
            database.upload_image(name, empPicture)
            os.remove(pic)
    return redirect(url_for('profile', name=name))


@app.route('/profile/<name>', methods=['GET', 'POST'])
def profile(name, lip=None):
    statusInfo = database.getStatusList(name)
    statusActive = statusInfo[0][1]
    if(statusActive != "on"):
        return redirect(url_for('launch', error="Sign in"))
    response = database.getCourses(name)
    info = database.getInfo(name)
    image_blob = database.readBLOB(name)
    if type(image_blob) is str:
        empPicture = convertToBinaryData("profile.png")
        image = b64encode(empPicture).decode("utf-8")
    else:
        image = b64encode(image_blob).decode("utf-8")

    myCourses = configureResponse(response)
    jsonCourses = {}
    jsonCourses['courses'] = myCourses
    return render_template('user.html', info=[name, info[0][0], jsonCourses], image=image)


@app.route("/upload", methods=["GET", "POST"])
def upload_image():
    if request.method == 'POST':
        print(request.args)
    return render_template("user.html", name=None)


@app.route('/image', methods=['GET', 'POST'])
def image():
    if request.method == 'POST':
        data = request.form['data']
        info = json.loads(data)
        message = database.upload_image(info[0], info[1])
    return jsonify([message])


@app.route('/course', methods=['GET', 'POST'])
def course():
    if request.method == 'POST':
        data = request.form['data']
        parsedData = json.loads(data)
        response = database.addCourse(parsedData[0], parsedData[1])
        return jsonify([response])


@app.route('/removeCourse', methods=['GET', 'POST'])
def removeCourse():
    if request.method == 'POST':
        data = request.form['data']
        parsedData = json.loads(data)
        stripCourse1 = parsedData[1].strip()
        stripCourse2 = stripCourse1.strip("'")
        response = database.deleteCourse(parsedData[0], stripCourse2)
        return jsonify([response])


def verify(useremail, password):
    user_list = database.get_user(useremail)
    if '@mun' not in useremail:
        message = 'Invalid email. Please try again'
    else:
        if len(user_list) == 0:
            message = 'No record of this email'
        elif useremail == user_list[0][0] and password != user_list[0][1]:
            message = ' Incorrect password. Try again'
        else:
            message = None
            database.updateStatus(useremail, 'on')
            session['user'] = useremail
    return message

@app.route('/getCourses', methods=['GET', 'POST'])
def getCourses():
    if request.method == 'POST':
        data = request.form['data']
        parsedData = json.loads(data)
        response = database.getCourses(parsedData[0])
        return jsonify([response])

# Messaging


def messageReceived(methods=['GET', 'POST']):
    print("Message received")


@socket.on('my event')
def handleEvent(json, methods=['GET', 'POST']):
    print('received {}'.format(str(json)))
    socket.emit('my response', json)

@socket.on('disconnect')
def disconnect_user():
    database.updateStatus(session['user'], 'off')

@app.route('/courseMembers', methods=['GET', 'POST'])
def getMembers():
    if request.method == 'GET':
        data = request.args['data']
        parsedData = json.loads(data)
        response = database.listCourseMates(parsedData[0].lstrip())
        res = {}
        for user in response:
            name = '{} {}'.format(user[0], user[1])
            mail = user[2]
            statusInfo = database.getStatusList(mail)
            statusActive = statusInfo[0][1]
            res[name] = statusActive
        return jsonify(res)

@app.route('/logout/<email>')
def logout(email):
    database.updateStatus(email, 'off')
    # Redirect to login page
    return redirect(url_for('launch', error=""))

if __name__ == "__main__":
    socket.run(app, debug=True)
